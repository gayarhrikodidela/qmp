import json as json1
from collections import OrderedDict
from functools import wraps

import jwt
import pandas as pd
import pyodbc
import requests
from flask import Flask, request
from flask_cors import CORS, cross_origin
import datetime
from functions_timeline import *
from login_screen_apis import *
from metric_list_apis import *
from metric_performance_apis import *
from metric_view_screen4_apis import *
from rr_matrix_apis import *
from report_screen_apis import *
from user_persona_apis import *

# app = Flask(__name__)
app = Flask(__name__, static_folder="../build", static_url_path="/")

CORS(app, support_credentials=True)
app.config["SECRET_KEY"] = "ac61805910e89ba8343d30f0364e1c6f"

@app.errorhandler(404)   
def not_found(e):   
    return app.send_static_file('index.html')

def token_required(f):
    @wraps(f)
    def decorator(*args, **kwargs):
        token = None
        if "x-access-tokens" in request.headers:
            token = request.headers["x-access-tokens"]
        if not token:
            return "Invalid Token", 400
        try:
            data = jwt.decode(token, app.config["SECRET_KEY"], algorithms=["HS256"])
            # current_user = Users.query.filter_by(public_id=data['uname']).first()
            cnxn = pyodbc.connect(
                "DRIVER={ODBC Driver 17 for SQL Server};SERVER="
                + server
                + ";DATABASE="
                + database
                + ";UID="
                + username
                + ";PWD="
                + password
            )
            cursor = cnxn.cursor()
            cursor.execute(
                """SELECT distinct [ID] FROM dbo.user_ref WHERE [ID] = ?""",
                (data["userid"]),
            )
            firstresult = cursor.fetchall()
            row = firstresult[0][0]
        except:
            return "Invalid Token", 400
        return f(row, *args, **kwargs)

    return decorator


@app.route("/")
@cross_origin(origin="*")
def home():
    return app.send_static_file("index.html")


@app.route("/loginuser", methods=["POST"])
def loginuser():
    variables = request.get_json()
    uname = variables["username"]
    pswd = variables["password"]
    message = login_function(uname, pswd)
    if message.isnumeric():
        #result = status_object("User not in system. Please check details and try again")
        token = jwt.encode(
            {
                "userid": message,
                "exp": datetime.datetime.utcnow() + datetime.timedelta(minutes=240),
            },
            app.config["SECRET_KEY"],
            "HS256",
        )
        result = status_object(token)
        return result
    elif message == 'Not in system':
        result = status_object('User not in system. Please check details and try again', 'Unsuccessful', 'Not Completed')
        return result
    elif message == 'Incorrect data':
        result = status_object('Username or password incorrect', 'Unsuccessful', 'Not Completed')
        return result


@app.route("/displaydata", methods=["GET"])
@token_required
def displaydata(*args):
    js = displaydatafunction()
    result = status_object(js)
    return result


@app.route("/userrole", methods=["GET"])
@token_required
def getroles(firstresult):
    roles = get_roles(firstresult)
    result = status_object(roles)
    return result


@app.route("/addmetric", methods=["POST"])
@token_required
def addmetric(firstresult):
    variables = request.get_json()
    category = variables["category"]
    metric_name = variables["metric_name"]
    lead_lag = variables["leading_lagging"]
    uom = variables["uom"]
    year = variables["year"]
    product_service = variables["product/service"]
    roles = variables["roles"]
    comment = variables["comment"]
    js = addnewmetric(category, metric_name, lead_lag, uom, year, product_service, comment, firstresult)
    result = status_object(js)
    return result


@app.route("/addsegment", methods=["POST"])
@token_required
def addsegment(firstresult):
    variables = request.get_json()
    segment = variables["segment"]
    comment = variables["comment"]
    product_service = variables["product/service"]
    message = addnewsegment(segment, comment, product_service, firstresult)
    result = status_object()
    return result


@app.route("/deletemetric", methods=["POST"])
@token_required
def deletemetric(firstresult):
    variables = request.get_json()
    metric_id = variables["metric_id"]
    comment = variables["comment"]
    message = updatedatafunction(metric_id, comment, firstresult)
    result = status_object()
    return result


@app.route("/deletesegment", methods=["POST"])
@token_required
def deletesegment(firstresult):
    variables = request.get_json()
    segment = variables["segment"]
    comment = variables["comment"]
    message = delete_segment(segment, comment, firstresult)
    result = status_object()
    return result


@app.route("/edittarget", methods=["POST"])
@token_required
def edittarget(firstresult):
    variables = OrderedDict(request.get_json())
    result = edit_targets(variables, firstresult)
    result = status_object()
    return result


@app.route("/copymetrics")
@token_required
def copymetrics(firstresult):
    message = copy_metrics(firstresult)
    result = status_object()
    return result


@app.route("/rrmatrix_display_data")
@token_required
def displaydata_rrmatrix(firstresult):
    year = request.args.get("year")
    category = request.args.get("category")
    df = displaydatafunction_rrmatrix(year,category)
    js = convert_to_nested(df)
    result = status_object(js)
    return result


@app.route("/rrmatrix_editroles", methods=["POST"])
@token_required
def editroles_rrmatrix(firstresult):
    data = OrderedDict(request.get_json())
    df = convert_to_flat(data)
    js = edit_role(df, firstresult)
    update = rr_matrix_update(data)
    result = status_object()
    return result


@app.route("/yearfilter")
@token_required
def year_filter(firstresult):
    js = yearfilter()
    result = status_object(js)
    return result


@app.route("/yearselection", methods=["GET"])
@token_required
def year_selection(firstresult):
    year = request.args.get("year")
    category = request.args.get("category")
    df = filtervalues(year, category)
    js = filterdf(df, year, category)
    result = status_object(js)
    return result


@app.route("/metricsinputscreen", methods=["POST"])
@token_required
def metrics_input(firstresult):
    df = displaydata_metricperformance(firstresult)
    js = df.to_json(orient = 'records')
    result = status_object(js)
    return result


@app.route("/editperformance", methods=["POST"])
@token_required
def editperformance(firstresult):
    variables = OrderedDict(request.get_json())
    result = edit_performance(variables, firstresult)
    result = status_object()
    return result


@app.route("/displaydatascreen4")
@token_required
def displaydatascreen4(firstresult):
    df = displaydatafunction_screen4()
    result = status_object(df)
    return result


@app.route("/editscreen4", methods=["POST"])
@token_required
def editscreen4(firstresult):
    variables = OrderedDict(request.get_json())
    metric_id = variables["Metric_ID"]
    segment = variables["Segment"]
    category = variables["Category"]
    metric_name = variables["Metric_Name"]
    leading_lagging = variables["Leading/Lagging"]
    uom = variables["UOM"]
    year = variables["Year"]
    product_service = variables["Product/Service"]
    pt = variables["Performance_Target"]
    pyq4p = variables["Previous_Year_Q4_Performance"]
    pyq4in = variables["Previous_Year_Q4_Insights"]
    q1p = variables["Q1_performance"]
    q1in = variables["Q1_Insights"]
    q2p = variables["Q2_performance"]
    q2in = variables["Q2_Insights"]
    q3p = variables["Q3_performance"]
    q3in = variables["Q3_Insights"]
    q4p = variables["Q4_performance"]
    q4in = variables["Q4_Insights"]
    result = edit_screen_4(variables, firstresult, metric_id, segment, category, metric_name, leading_lagging, uom, year, product_service, pt, pyq4p, pyq4in, q1p, q1in, q2p, q2in, q3p, q3in, q4p, q4in)
    result = status_object()
    return result


@app.route("/yearfilterscreen4", methods=["GET"])
@token_required
def year_selection_screen4(firstresult):
    year = request.args.get("year")
    category = request.args.get("category")
    js = filterdata(year,category)
    result = status_object(js)
    return result


@app.route("/displaytimeline")
@token_required
def displaytimeline(firstresult):
    js = displaytimelinefunction()
    result = status_object(js)
    return result


@app.route("/edittimeline", methods=["POST"])
@token_required
def edittimeline(firstresult):
    variables = OrderedDict(request.get_json())
    result = update_timelines(variables, firstresult)
    js = updated_row(variables)
    result = status_object(js)
    return result


@app.route("/displaydata_screen6", methods=["GET"])
@token_required
def displaydatareport(firstresult):
    yr = request.args.get("year")
    qtr = request.args.get("quarter")
    category = request.args.get("category")
    js = displaydata_detailedreport(yr, qtr, category)
    result = status_object(js)
    return result


@app.route("/categoryfilter")
@token_required
def categoryget(firstresult):
    js = category()
    result = status_object(js)
    return result


@app.route("/displaydata_overviewreport", methods=["GET"])
@token_required
def overview_report(firstresult):
    yr = request.args.get("year")
    qtr = request.args.get("quarter")
    js1 = displaydata_detailedreport(yr, qtr, 'Product')
    js2 = displaydata_detailedreport(yr, qtr, 'Service')
    data = {}
    data["Product"] = js1
    data["Service"] = js2
    result = status_object(data)
    return result


@app.route("/display_system_users", methods=["GET"])
@token_required
def displayuserscreen(firstresult):
    js = display_users()
    result = status_object(js)
    return result


@app.route("/add_new_user", methods=["POST"])
@token_required
def new_user(firstresult):
    variables = OrderedDict(request.get_json())
    message = addnewuser(variables)
    result = status_object()
    return result
        

@app.route("/segment_list", methods=["GET"])
@token_required
def seg_list(firstresult):
    js = segment_list()
    result = status_object(js)
    return result


@app.route("/deleteuser", methods=["POST"])
@token_required
def deleteuser(firstresult):
    variables = request.get_json()
    u_id = variables["u_id"]
    message = delete_user(u_id)
    result = status_object()
    return result


@app.route("/edituser", methods=["POST"])
@token_required
def edituser(firstresult):
    variables = request.get_json()
    u_mail = variables["u_mail"]
    u_name = variables["u_name"]
    u_id = variables["u_id"]
    message = edit_user(u_mail, u_name, u_id)
    result = status_object()
    return result



@app.route("/users_list", methods=["GET"])
@token_required
def u_list(firstresult):
    js = users_list()
    result = status_object(js)
    return result


if __name__ == "__main__":
    app.run(debug=True)
