import pyodbc
import pandas as pd
import json
import datetime
from datetime import date
from collections import OrderedDict

#When in doubt, print it out.

server = 'mars-horizon.database.windows.net'
database = 'marsanalyticsdevsqlsrv'
username = 'mthco'
password = 'Mathco_123'
#server = 'marsanalyticsdevsqlsrv.database.windows.net'
#database = 'qmpdevsqldb'
#username = 'BI_User'
#password = 'B}Gvd,9@pHW%5(b6'


def displaydatafunction_screen4():
    #conn through pyodbc execute through pandas
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query('''SELECT * FROM dbo.tbl_screen_3_V2 order by Segment where is_disabled = 0''',cnxn)
    cnxn.close()
    #getting column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='tbl_screen_3_V2'")
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.close()
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns_df)
    req_json = df.to_json(orient = 'records')
    return req_json


#function to convert dataframe to json
def dataframetojson(df):
    req_json = df.to_json()
    return req_json


def edit_screen_4(variables, firstresult, metric_id, segment, category, metric_name, leading_lagging, uom, year, product_service, pt, pyq4p, pyq4in, q1p, q1in, q2p, q2in, q3p, q3in, q4p, q4in):
    column_names = list(variables.keys())
    #get old value of metric
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    old_value_query = pd.read_sql_query(("SELECT * FROM dbo.tbl_screen_3_V2 WHERE Metric_ID = " + str(variables['Metric_ID']) + " and Segment = "+ repr(variables['Segment'])), cnxn)
    cnxn.commit()
    cnxn.close() # Close Connection
    #convert old value and new value to json and add to history table
    df = pd.DataFrame(old_value_query, columns= column_names) 
    js = df.to_json(orient = 'records') #convert to json
    new_js = json.dumps(variables)
    #upate history table
    query = 'Select distinct Username from dbo.tbl_users where [User ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.edit_screen4_history(old_value, new_value, when_updated, updated_by) values (?, ?, ?, ?)", js, new_js, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #Running SP
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "EXEC [dbo].[EDIT_SCREEN_4] @METRIC_ID = ?, @SEGMENT = ?, @CATEGORY = ?, @METRIC_NAME = ?, @Leading_Lagging = ?, @UOM = ?, @YEAR = ?, @PRODUCT_SERVICE = ?, @PT = ?, @PYQ4P = ?, @PYQ4IN = ?, @Q1P = ?, @Q1IN = ?, @Q2P = ?, @Q2IN = ?, @Q3P = ?, @Q3IN = ?, @Q4P = ?, @Q4IN = ?"
    params = (metric_id, segment, category, metric_name, leading_lagging, uom, year, product_service, pt, pyq4p, pyq4in, q1p, q1in, q2p, q2in, q3p, q3in, q4p, q4in)
    cursor.execute(sqlExecSP, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close()
    return "Values Updated Successfully!!!"
    
    
def filterdata(year,category):
    query = 'select * from dbo.tbl_screen_3_V2 where Year = ' + repr(year) + 'and [Product/Service] = ' + repr(category)
    columns_query = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='tbl_screen_3_V2'"
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(query,cnxn)
    cnxn.close()
    #Get column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(columns_query)
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.close()
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns_df)
    js = df.to_json(orient = 'records')
    return js


