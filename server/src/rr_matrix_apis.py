import pandas as pd
from pandas.io import json
import pyodbc
import datetime
from datetime import date
from flask import jsonify
from collections import OrderedDict

#When in doubt, print it out.

server = 'mars-horizon.database.windows.net'
database = 'marsanalyticsdevsqlsrv'
username = 'mthco'
password = 'Mathco_123'
#server = 'marsanalyticsdevsqlsrv.database.windows.net'
#database = 'qmpdevsqldb'
#username = 'BI_User'
#password = 'B}Gvd,9@pHW%5(b6'


#function to get data from RRMatrix table:
def displaydatafunction_rrmatrix(year = 'None',category = 'None'): 
    current_date = date.today()
    current_year = current_date.year
    #Generate queries
    if year == 'None' and category == 'None':
        query = "SELECT * FROM dbo.RR_Matrix where [Is_disabled] = 0 and [Product/Service] = 'Product' and Year = " + repr(current_year)
        # columns_query = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='RR_Matrix'"
    else:
        query = "Select * from dbo.RR_Matrix where [Is_disabled] = 0 and Year = " + f"{year}" + " and [Product/Service] = " + repr(category)
        # columns_query = "select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='RR_Matrix'"
    #Get data
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    sql_query = pd.read_sql_query(query,cnxn)
    cnxn.close()
    # #conn through pyodbc execute through pandas
    # cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    # sql_query = pd.read_sql_query(f'''SELECT * FROM dbo.RR_Matrix where [Product/Service] = 'Product' and Year = {repr(current_year)}''',cnxn)
    # cnxn.close()
    #getting column names
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute("select COLUMN_NAME from INFORMATION_SCHEMA.COLUMNS where TABLE_NAME='RR_Matrix'")
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_df = [item for sublist in column_name for item in sublist]
    cnxn.close()
    #filter segments based on product/service
    if year == 'None' and category == 'None':
        query_2 = "Select Segment from dbo.segment_ref where [Product/Service] = 'Product' and [Is Disabled] = 0 and Year = " + repr(current_year)
    else:
        query_2 = "Select Segment from dbo.segment_ref where [Is disabled] = 0 and Year = " + f"{year}" + " and [Product/Service] = " + repr(category)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query_2)
    column_name = []
    for row in cursor:
        row_to_list = [elem for elem in row]
        column_name.append(row_to_list)
    columns_to_keep = [item for sublist in column_name for item in sublist]
    print(columns_to_keep)
    columns = columns_df[0:4]
    segment_columns = []
    for i in columns_df:
        for j in columns_to_keep:
            # print(j)
            if j in i:
                segment_columns.append(i)
    columns = columns + segment_columns
    # print(columns)
    #converting to df
    df = pd.DataFrame(sql_query, columns= columns)
    # print(df)
    return df
    # #df.drop(columns= ['Is_disabled'], inplace=True)
    # #convert to json
    # js = df.to_json(orient = 'records')
    # return js
   
#function to convert data into nested json
def convert_to_nested(df):
    final_dict = []
    dict_list = df.to_dict('records')
    for row in dict_list:
        metrics_input = {}
        segment_insights = {}
        target_definition = {}
        json_format = {}
        for column in row.keys():
            if "Metrics_Input" in column:
                metrics_input[column] = row[column]
            elif "Segment_Insights" in column:
                segment_insights[column] = row[column]
            elif "Target_Definition" in column:
                target_definition[column] = row[column]
            else:
                json_format[column] = row[column]
        json_format['metrics_input'] = metrics_input
        json_format['segment_insights'] = segment_insights
        json_format['target_definition'] = target_definition
        final_dict.append(json_format)
    final_json = json.dumps(final_dict)
    # print (final_json)
    return final_json

#function to convert nested dict to flat dict
def convert_to_flat(nested_dict):
    flat_dict = {}
    for key, val in nested_dict.items():
        if type(val) == dict:
            flat_dict.update(val)
        else:
            flat_dict[key] = val
    return flat_dict


#function to edit metric roles
def edit_role(variables, firstresult):
    column_names = list(variables.keys())
    #get old value of metric
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    old_value_query = pd.read_sql_query(("SELECT * FROM dbo.RR_Matrix WHERE ID = " + str(variables['ID'])), cnxn)
    cnxn.commit()
    cnxn.close() # Close Connection
    #convert old value and new value to json and add to history table
    df = pd.DataFrame(old_value_query, columns= column_names) 
    js = df.to_json(orient = 'records') #convert to json
    new_js = json.dumps(variables)
    #upate history table
    query = 'Select distinct Username from dbo.tbl_users where [User ID] = ' + repr(firstresult)
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.execute(query)
    result = cursor.fetchall()
    user = result[0][0]
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    when = datetime.datetime.utcnow()
    cursor.execute("insert into dbo.edit_role_history(old_value, new_value, when_updated, updated_by) values (?, ?, ?, ?)", js, new_js, when, user)
    cnxn.commit()
    cursor.close()
    del cursor
    cnxn.close()
    #generating the update query
    query = "UPDATE dbo.RR_Matrix SET "
    for i in column_names:
        if i != 'ID' and i!= 'Is_disabled':
            x = '[' + i + ']' + ' = ' + ' ?, '
            query = query + x 
    query = query + 'Is_disabled = 0 ' + f"WHERE ID = {variables['ID']}"
    params = []
    for i in range(1, len(column_names)):
        if column_names[i] != 'Is_disabled': 
            params.append(variables[column_names[i]])
    #updating table with new values
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    cursor.execute(query, params)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    return "Role Updated Successfully!!!"


#Function to store updated RR Matrix values
def rr_matrix_update(variables):
    row_values = []
    for i in variables.keys():
        if type(variables[i]) == dict:
            for value in variables[i].keys():
                segment = value.split('_')[0]
                if i == 'metrics_input' and segment == 'Corporate':
                    persona = 'Corporate Data'
                elif i == 'metrics_input' and segment != 'Corporate':
                    persona = 'Segment Data'
                elif i == 'segment_insights' and segment == 'Corporate':
                    persona = 'Corporate Insights'
                elif i == 'segment_insights' and segment != 'Corporate':
                    persona = 'Segment Insights'
                else:
                    persona = 'Target Definition'
                users_assigned = variables[i][value].split(';')
                for user in users_assigned:
                    rows = []
                    rows.append(variables['ID'])
                    rows.append(user)
                    rows.append(persona)
                    rows.append(segment)
                    rows.append(variables['Year'])
                    row_values.append(tuple(rows))
    insert_query = '''Insert into dbo.temp_rr_matrix_update
                      (MetricID, UserName, Persona, Segment, Year)
                      values (?,?,?,?,?)'''   
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)  
    cursor = cnxn.cursor()
    cursor.executemany(insert_query, row_values)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    #executing SP
    cnxn = pyodbc.connect('DRIVER={ODBC Driver 17 for SQL Server};SERVER='+server+';DATABASE='+database+';UID='+username+';PWD='+ password)
    cursor = cnxn.cursor()
    sqlExecSP = "EXEC [dbo].[UPDATE_ROLES]"
    cursor.execute(sqlExecSP)
    cnxn.commit()
    cursor.close() # Close and delete cursor
    del cursor
    cnxn.close() # Close Connection
    return "Completed"
