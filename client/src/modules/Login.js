import React, { useState } from 'react';
import Common from '../utils/Common';
import HomeService from '../services/HomeService';
import Spinner from 'react-bootstrap/Spinner'
import {
  useHistory
} from "react-router-dom";
function Login() {
  const commonService = new Common;
  const homeService = new HomeService
  const username = useFormInput('');
  const password = useFormInput('');
  const [error, setError] = useState(null);
  const [loading, setLoading] = useState(false);
  const history = useHistory();
  // handle button click of login form
  const handleLogin = () => {
    console.log(username.value);
    console.log(password.value);
    setLoading(!loading);
    var loginDetails = {
      username: username.value,
      password: password.value
    }
    homeService.loginUser(loginDetails).then(item => {
      console.log(item);
      if (item.status == "Success") {
        homeService.getUserRoles(item.data).then(user => {
          console.log(user);
          setLoading(!loading);
          var data = JSON.parse(user.data);
          console.log(data);
          let obj = {
            userName: username.value,
            password: password.value,
            roles: data
          }

          commonService.setUserSession(item.data, obj);
          history.push('/mainMenu');

        })
        //commonService.setUserSession(item.data,loginDetails);
      }
      else
        if (item.status == "Unsuccessful") {
          setLoading(false)
          alert("Username and Password do not match");
        }

      //   var data = JSON.parse(item.data);
      //   console.log(data);
      //   let obj = {
      //   userName: username.value,
      //   password: password.value,
      //   segmentRole: data.Segment,
      //   metricRole:data.Metric_ID,
      //   Metric_Type: data.Metric_Type
      // }

      // commonService.setUserSession("12345",obj);
      // history.push('/mainMenu');
    })
  }

  const handleKeypress = (e) => {
    if (e.charCode === 13) {
      handleLogin();
    }
  };

  const loginButton = {
    margin: "5px",
    color: "#fff",
    background: "rgb(56, 96, 178)",
    border: "1px solid",
    borderRadius: "6px",
    padding: "6px",
    fontSize: "16px",
    width: "20%"
  }
  return (
    <div style={{ width: "40%", margin: "5% auto", border: "1px solid" }}>
      {loading && <div className="loader-box">
        <div id="loader"></div>
      </div>}
      <h3 style={{ textAlign: "center", background: "#0000a0", color: "white", margin: "0", padding: "10px" }}>Login</h3>
      <div style={{ width: "90%", margin: "5% auto" }}>
        <div>
          <h3 style={{ margin: "5px 0" }}>Username</h3>
          <input type="text" style={{ width: "95%", height: "20px" }}{...username} autoComplete="new-password" />
        </div>
        <div style={{ marginTop: 10 }}>
          <h3 style={{ margin: "5px 0" }}>Password</h3>
          <input type="password" onKeyPress={handleKeypress} style={{ width: "95%", height: "20px" }}{...password} autoComplete="new-password" />
        </div>
        {/* {error && <><small style={{ color: 'red' }}>{error}</small><br /></>}<br /> */}
        <div style={{ textAlign: "center" }}>
          <input type="button" style={loginButton} value={loading ? 'Loading...' : 'Login'} onClick={handleLogin} disabled={loading} /><br />
        </div>
      </div>
    </div>
  );
}

const useFormInput = initialValue => {
  const [value, setValue] = useState(initialValue);

  const handleChange = e => {
    setValue(e.target.value);
  }
  return {
    value,
    onChange: handleChange
  }
}

export default Login;