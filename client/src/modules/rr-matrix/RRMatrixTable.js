import React from "react";
import { Table } from 'react-bootstrap';
import EditRole from './EditRole';
import HomeService from "../../services/HomeService";
import Common from "../../utils/Common";
import MetricNameDropDown from "../MetricNameDropDown";
class RRMatrixTable extends React.Component {

    constructor() {
        super();
        this.state = {
            metrixTable: [],
            subHeading: [],
            popUpModelOpen: false,     
            subHeadingCount: 0,
            tableData: [],
            showEditRole: false,
            metrixToEdit: {},
            loading: true,
            selectedYear:"",
            selectedType: "Product",
            year:[],
            isAdmin: false,
            userList:[],
            segmentRole:[],
            metricRole:[],
            targetRole:[],
            segment:[]
        }
        this.toggleShowEditRole = this.toggleShowEditRole.bind(this);
        this.setLoading = this.setLoading.bind(this);
        this.filterDataByYearOrType = this.filterDataByYearOrType.bind(this);
        this.filterDataSegment = this.filterDataSegment.bind(this);
        this.homeService = new HomeService();
        this.commonSerice = new Common();
    }
    setLoading() {
        this.setState({ loading: !this.state.loading });
    }
    editMetricRole = item => {
        console.log(item);
        var segmentRole = [];
        var metricRole = [];
        var targetRole = [];
        for (var key in item.segment_insights) {
            var name = key.split("_");
            var obj = {};
            obj.name = name[0];
            var names = item.segment_insights[key];
            if(names != null){
              var value = names.split(";").map(item => item.trim());
              var selectedNames = [];
              value.forEach(element => {
                selectedNames.push( { label: element, value: element })
              });
              obj.value = selectedNames
            }else{
              obj.value = []
            }
            segmentRole.push(obj);
          }
          for (var key in item.metrics_input) {
            var name = key.split("_");
            var obj = {};
            obj.name = name[0];
            var names = item.metrics_input[key];
            if(names != null){
                var value = names.split(";").map(item => item.trim());
                var selectedNames = [];
                value.forEach(element => {
                  selectedNames.push( { label: element, value: element })
                });
                obj.value = selectedNames
              }else{
                obj.value = []
              }
            metricRole.push(obj);
          }
          for (var key in item.target_definition) {
            var name = key.split("_");
            var obj = {};
            obj.name = name[0];
            var names = item.target_definition[key];
            if(names != null){
                var value = names.split(";").map(item => item.trim());
                var selectedNames = [];
                value.forEach(element => {
                  selectedNames.push( { label: element, value: element })
                });
                obj.value = selectedNames
              }else{
                obj.value = []
              }
            targetRole.push(obj);
          }
        this.setState({ metrixToEdit: item ,segmentRole:segmentRole,metricRole:metricRole,targetRole:targetRole});
        this.toggleShowEditRole();
    }
    toggleShowEditRole() {
        this.setState({ showEditRole: !this.state.showEditRole, popUpModelOpen: !this.state.popUpModelOpen });
    }
    componentDidMount() {
        this.setState({ selectedYear: new Date().getFullYear() })
        this.getMetrixData(new Date().getFullYear(), this.state.selectedType);
        this.getYearList();
        this.commonSerice.isAdmin().then(role =>{
            console.log("User role is admin:"+role);
            if(role === "true"){
                this.setState({isAdmin: true});
                this.homeService.getUserList().then(item => {
                    if (item.status == 'Success') {
                      var userList = [];
                      let data = JSON.parse(item.data);
                      console.log(data);
                      data.forEach(e=>{
                        userList.push({ label: e[0], value: e[0] });
                      })
                      this.setState({ ...this.state, userList: userList });
                    }
                  })
              }
          })
    }
    getYearList() {
       
        this.homeService.getYearFilter().then(item => {
          if (item.status == 'Success') {
            let data = JSON.parse(item.data);
            this.setState({ ...this.state, year: data });
          }
        })
      }
    filterDataByYearOrType(event){
        var year;
        var metricType;
        if(event.target.value === "Service" || event.target.value === "Product"){
          this.setState({ selectedType: event.target.value });
         metricType = event.target.value
         year = this.state.selectedYear;
        }else{
          this.setState({ selectedYear: event.target.value });
          metricType = this.state.selectedType
          year = event.target.value;
        }

        this.setLoading();
        // console.log(this.state.selectedYear+""+this.state.selectedMetricType)
        this.getMetrixData(year,metricType);
    }
    
    filterDataSegment(segments) {
    console.log(segments)
    let data = [...this.state.metrixTable];
        console.log(this.state.metrixTable);
        let subHeading = [];
        var subHeadingCount = 0;
        var segment = [];
        console.log(data);
        if(segments.length === 0){
            segments = [...this.state.segment];
        }
        if(data.length > 0){
            // segments.forEach(seg =>{
                for (var key in data[0].metrics_input) {
                    var heading = key.split("_");
                    var index = segments.findIndex(e=> key.includes(e.value))
                    if(index > -1){
                        subHeading.push(heading[0])
                    }
                }
                for (var key in data[0].segment_insights) {
                    var heading = key.split("_");
                    var index = segments.findIndex(e=> key.includes(e.value))
                    if(index > -1){
                        subHeading.push(heading[0])
                    }
                }
                for (var key in data[0].target_definition) {
                    var heading = key.split("_");
                    var index = segments.findIndex(e=> key.includes(e.value))
                    if(index > -1){
                        subHeading.push(heading[0])
                    }
                }
            console.log(subHeading);
            var subHeadingCount = subHeading.length / 3;

        data.forEach(element => {
            let metricInput = [];
            let segmentInput = [];
            let targetDefination = [];
            for (var key in element.metrics_input) {
                var index = segments.findIndex(e => key.includes(e.value));
                if(index > -1){
                    metricInput.push(element.metrics_input[key])
                }
                
            }
            for (var key in element.segment_insights) {
                var index = segments.findIndex(e => key.includes(e.value));
                if(index > -1){
                segmentInput.push(element.segment_insights[key])
                 }
            }
            for (var key in element.target_definition) {
                var index = segments.findIndex(e => key.includes(e.value));
                if(index > -1){
                targetDefination.push(element.target_definition[key])
                }
            }
            element.metricInput = metricInput;
            element.segmentInput = segmentInput;
            element.targetDefination = targetDefination;
        })
        console.log(data);
       }
       this.setState({ subHeadingCount: subHeadingCount, subHeading: subHeading, tableData: data});

    }
    getMetrixData(year,type) {
        var obj ={
            year: year,
            category: type
        }
        //this.setLoading();
        this.homeService.getRrmatrixDisplayData(obj).then(item => {
            this.setLoading();
            if (item.status === 'Success') {
                let data = JSON.parse(item.data);
                console.log(data);
                this.setState({ ...this.state, metrixTable: data })
                    this.processTableData(this.state.metrixTable);
                
            }
        });
    }
    processTableData() {
        let data = [...this.state.metrixTable];
        console.log(this.state.metrixTable);
        let subHeading = [];
        var subHeadingCount = 0;
        var segment = [];
        console.log(data);
        if(data.length > 0){
            for (var key in data[0].metrics_input) {
                var heading = key.split("_");
                subHeading.push(heading[0])
            }
            for (var key in data[0].segment_insights) {
                var heading = key.split("_");
                subHeading.push(heading[0])
            }
            for (var key in data[0].target_definition) {
                var heading = key.split("_");
                subHeading.push(heading[0])
            }
        
        var subHeadingCount = subHeading.length / 3;
        data.forEach(element => {
            let metricInput = [];
            let segmentInput = [];
            let targetDefination = [];
            for (var key in element.metrics_input) {
                metricInput.push(element.metrics_input[key])
            }
            for (var key in element.segment_insights) {
                segmentInput.push(element.segment_insights[key])
            }
            for (var key in element.target_definition) {
                targetDefination.push(element.target_definition[key])
            }
            element.metricInput = metricInput;
            element.segmentInput = segmentInput;
            element.targetDefination = targetDefination;
        })
        subHeading.forEach(heading =>{
            var index = segment.findIndex(e=> e.value === heading);
            if(index === -1){
                segment.push({ label: heading, value: heading })
            }
        })
       }
        this.setState({ subHeadingCount: subHeadingCount, subHeading: subHeading, tableData: data, segment: segment });

    }
    saveEditedRoles(role){
        console.log(role);
        var data = [...this.state.tableData];
        var index = data.findIndex(e => e.ID === role.ID);
        if(index > -1){
            data.splice(index,1,role);
        }
        console.log(data);
        this.setState({tableData: data});
        this.toggleShowEditRole();
    }
    render() {

        return (
            
            <div style={{ overflow: "auto", height: "80vh" }}>
               { this.state.loading && <div className="loader-box">
                <div id="loader"></div>
                </div>}
                <div className="filter">
                    <div className="filterDiv">
                        <label>Year</label>
                        <select className="filterDropDown" onChange={this.filterDataByYearOrType} value={this.state.selectedYear}>
                            {
                                this.state.year.map((y) => {
                                    return (<option key={y} value={y}>{y}</option>)
                                })
                            }
                        </select>
                    </div>
                    <div className="filterDiv">
                        <label>Metric Type</label>
                        <select className="filterDropDown" onChange={this.filterDataByYearOrType} value={this.state.selectedType}>
                            <option value="Product">Product</option>
                            <option value="Service">Service</option>
                        </select>
                    </div>
                    <div className="filterDiv">
                        <label>Segments</label>
                        <MetricNameDropDown metricNameList={this.state.segment}
                            filterMetricByName={this.filterDataSegment.bind(this)} ></MetricNameDropDown>
                    </div>
                </div>
                <Table responsive style={{ width: "100%" }}>
                    <thead>
                        <tr>
                            
                        <th style={{...(this.state.popUpModelOpen  ? {position : "inherit"}:{})}} className="fixedHeader" rowSpan="2">Category</th>
                            <th style={{...(this.state.popUpModelOpen  ? {position : "inherit"}:{left:"72px"})}} className="fixedHeader" rowSpan="2">Metric</th>
                            <th colSpan={this.state.subHeadingCount}>Metrics Input</th>
                            <th colSpan={this.state.subHeadingCount}>Metrics Insights</th>
                            <th colSpan={this.state.subHeadingCount}>Targets Definition</th>
                            {this.state.isAdmin === true && <th rowSpan="2">Edit Roles</th>}
                        </tr>
                        <tr>
                            {this.state.subHeading.map((sub) => {
                                return (<th style={{"top": "26px"}}> {sub} </th>)
                            })}
                        </tr>
                    </thead>
                    <tbody>
                        {this.state.tableData.map((data) => {
                            return (<tr key={data.ID}>
                                <td className={ !this.state.popUpModelOpen ? "fixedRow":""} >{data.Category}</td>
                                <td className={ !this.state.popUpModelOpen ? "fixedRow":""} style={{left:"72px"}}>{data["Metric Name"]}</td>
                                
                                {
                                    data.metricInput.map(m => {
                                        return (<td>{m}</td>)
                                    })
                                }
                                {
                                    data.segmentInput.map(s => {
                                        return (<td>{s}</td>)
                                    })
                                }
                                {
                                    data.targetDefination.map(s => {
                                        return (<td>{s}</td>)
                                    })
                                }
                               {this.state.isAdmin === true && <td><button onClick={e => this.editMetricRole(data)}>Edit Roles</button></td> }
                            </tr>
                            )
                        })
                        }

                    </tbody>
                </Table>
                {this.state.showEditRole && <EditRole
                    handleClose={this.toggleShowEditRole}
                    metric={this.state.metrixToEdit}
                    segmentRole ={this.state.segmentRole}
                    metricRole = {this.state.metricRole}
                    targetRole = {this.state.targetRole}
                    userList={this.state.userList}
                    saveEditedRoles={this.saveEditedRoles.bind(this)}
                />}
            </div>

           
        );
    }


}

export default RRMatrixTable;