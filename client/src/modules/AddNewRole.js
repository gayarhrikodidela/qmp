import React , {useState} from "react";
import './Popup.css';
const AddNewRole = props => {
    const [heading, setHeading] = useState(props.heading);
    const [metricDetails, setMetricDetails] = useState(props.newMetric);
    const[role, setRole] = useState({})
    const changeInput = (value,heading) =>{
        role[heading] = value.target.value
        console.log(role);
    }
    const addNewRole = (value)=>{
       metricDetails.role = role;
       console.log(metricDetails);
       props.saveNewMetric(metricDetails);
    }
  return (
    <div className="popup-box">
      <div className="box">
          <div style={{padding: "15px", backgroundColor : "#8080cf"}}>
             <span className="close-icon" onClick={props.handleClose}>x</span>
             <h3 style={{margin: "0"}}>Add New Role</h3>
          </div>
        <div>
                
        </div>
        <div className="modal-box">
        <label className="input-label">
                 Year
              <input type="text" name="year" disabled value={metricDetails.year} />
            </label>
            <label className="input-label">
              Category
              <input type="text" name="metricName" disabled value={metricDetails.category}/>

            </label>
            <label className="input-label">
              Metric Name
              <input type="text" name="metricName" disabled value={metricDetails.metricName}/>
            </label>
            <label className="input-label">
              Leading/Lagging
              <input type="text" name="uom" disabled  value={metricDetails.leadLag}/>
              
            </label>
            <label className="input-label">
              UOM
              <input type="text" name="uom"  disabled value={metricDetails.uom}/>
            </label>
            {
                heading.map(v=>{
                   return ( <label className="input-label">
                     {v}
                    <input type="text" name={v} onChange={(value)=> changeInput(value,v)} />
                    </label> )
                })
            }
            
            <div style={{textAlign: "center"}}>
            <button onClick={(value)=> addNewRole(value)}>Save</button>
            <button onClick={props.handleClose}>Back</button>
            </div>
            
          </div>
      </div>
    </div>
  );
};
 
export default AddNewRole;