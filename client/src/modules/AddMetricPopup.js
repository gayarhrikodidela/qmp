import React, {useState} from "react";
import './Popup.css';
import Creatable from 'react-select/creatable'
import MultiSelect from "react-multi-select-component";
const AddMetricPopup = props => {
  const [productCategory, setProductCategory ] = useState(props.productCategory);
  const [serviceCategory, setServiceCategory ] = useState(props.serviceCategory);
  const [type, setType] = useState("Product")
  const [categoryValue, setCategoryValue ] = useState([]);
  const [newMetric, setNewMetric] = useState({year:new Date().getFullYear(),category:'',metric_name:'',leading_lagging:'',uom:'',roles:{}, "product/service":'Product'});
  console.log(productCategory);
  console.log(serviceCategory);
  const handleChange = (field, value) =>{
    console.log(value);
      switch (field){
        case 'category':
          setCategoryValue(value)
          if(value !== null){
            newMetric.category = value.value;
          }
          
          break
  
          default:
            break
      }
    
  }
  const setLeadLag = (value, option) => {
    console.log(option);
    newMetric.leading_lagging = option;
  }
  const setProductService = (value,option) =>{
    console.log(option);
    newMetric["product/service"] = option;
    setCategoryValue([]);
    setType(option);

  }
  const saveMetric = (value) =>{
    console.log(newMetric);
    props.saveNewMetric(newMetric);
  }
  const changeInput = (value,opt) => {
    if(opt == 'MetricName'){
        newMetric.metric_name = value.target.value
    }else if(opt === 'comment'){
      newMetric.comment = value.target.value
    }
    else{
      newMetric.uom = value.target.value
    }
  }
  const addNewRole = (value) =>{
    props.addNewRole(newMetric)
  }
  return (
    <div className="popup-box">
      <div className="box">
          <div style={{padding: "15px", backgroundColor : "8080cf"}}>
             <span className="close-icon" onClick={props.handleClose}>x</span>
             <h3 style={{margin: "0"}}>Add New Metric</h3>
          </div>
        <div>

        </div>
        <div className="modal-box">
            
            <label className="input-label">
                 Year
              <input type="text" name="year" disabled value={newMetric.year} />
            </label>
            <label className="input-label">
              Type
              <div style={{marginTop: "1%"}}>
              <input type="radio" id="Lead" defaultChecked name="Product_Service" value="Product" onChange={(value)=> setProductService(value,'Product')} />
              <label>Product</label>
              <input type="radio" id="css" name="Product_Service" value="Service" onChange={(value)=> setProductService(value,'Service')} />
              <label>Service</label>
              </div>
              
            </label>
            <label>
              Category
              {/* <MultiSelect
                options={category}
                 value={categoryValue}
                onChange={(value)=>handleChange('category',value)}
                labelledBy="Select"
                hasSelectAll = {true}
                shouldToggleOnHover={false}
              /> */}
              { type === "Product" &&
                <Creatable
              isClearable
              classNamePrefix="react-select"
              onChange={(value)=>handleChange('category',value)}
              options={productCategory}
              value={categoryValue}
              >
              </Creatable>
              }
              { type === "Service" &&
                <Creatable
              isClearable
              classNamePrefix="react-select"
              onChange={(value)=>handleChange('category',value)}
              options={serviceCategory}
              value={categoryValue}
              >
              </Creatable>
              }
              

            </label>
            <label className="input-label">
              Metric Name
              <input type="text" name="metricName" onChange={(value)=> changeInput(value,'MetricName')}/>
            </label>
            <label className="input-label">
              Leading/Lagging
              <div style={{marginTop: "1%"}}>
              <input type="radio" id="Lead" name="Leading_Lagging" value="Leading" onChange={(value)=> setLeadLag(value,'Lead')} />
              <label>Lead</label>
              <input type="radio" id="css" name="Leading_Lagging" value="Laging" onChange={(value)=> setLeadLag(value,'Lag')} />
              <label>Lag</label>
              </div>
              
            </label>
            
            <label className="input-label">
              UOM
              <input type="text" name="uom"  onChange={(value)=> changeInput(value,'UOM')}/>
            </label>
            <label className="input-label">
              Reason For Change
              <input type="text" name="comment"  onChange={(value)=> changeInput(value,'comment')}/>
            </label>
            <div style={{textAlign: "center"}}>
            <button onClick={(value)=> saveMetric(value)}>Save</button>
            <button onClick={props.handleClose}>Cancel</button>
            {/* <button  onClick={(value)=> addNewRole(value)}>Add New Role</button> */}
            
            </div>
            
          </div>
      </div>
    </div>
  );
};
 
export default AddMetricPopup;