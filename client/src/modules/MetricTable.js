import React, { useEffect, useState } from "react";
import { Table } from 'react-bootstrap';
import EditTarget from "./EditTarget";
import Modal from 'react-modal';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger'
import Tooltip from 'react-bootstrap/Tooltip'

const MetricTable = props => {

  const [tbody, setBody] = useState(props.body)
  const [modalIsOpen, setModalIsOpen] = useState(false);
  const [targetToEdit, setTargetToEdit] = useState({});
  // const [heading, setHeading] = useState([]);
  // const [subHeading, setSubHeading] = useState([]);
  let body = [...tbody];

  useEffect(() => {
    console.log("Use effect")
    setBody(props.body)
    body = [...tbody];
    console.log(tbody)
    //processData();
  }, [props.body]);
  console.log(body);
  const subHeading = [];
  const heading = []
  //body.forEach( e=>{
  for (var key in body[0]) {
    if (key !== 'Segment' && key !== 'ID' && key !== 'Year' && key !== 'comments' && key !== 'targets' && key !== 'Is_disabled' && key !== 'allowEdit'
      && key !== 'currentYearSelected' && key !== 'targetTemp' && key !== 'Product/Service') {
      var index = heading.findIndex(e => e === key);
      if (index === -1) {
        heading.push(key);
      }
    }
    else if (key === 'Segment') {
      body[0].Segment.forEach(seg => {
        for (var key in seg) {
          var index = heading.findIndex(e => e === key);
          if (index === -1) {
            heading.push(key);
          }
        }
      })
    }
  }
  console.log(heading);
  //})
  //if(subHeading.length !== (segmentHeading.length*3)){
  heading.forEach(e => {
    if (e !== 'Category' && e !== 'Metric Name' && e !== 'Leading\/Lagging' && e !== 'UOM' && e !== 'ID' && e !== 'Is_disabled'
      && e !== 'allowEdit' && e !== 'currentYearSelected'
      && !e.includes(".")) {
      subHeading.push('CYT');
      subHeading.push('PYT');
      subHeading.push('Q4PYPerf');
    }
  })
  //}

  // setHeading(head);
  // setSubHeading(subHead);
  body.forEach(e => {
    for (var key in e) {

      if (key === 'Segment') {
        const segment = []
        e.Segment.forEach(seg => {
          for (var key in seg) {
            segment.push(seg[key]);

          }
        })
        e["targets"] = segment;
        e["targetTemp"] = [...segment];
      }
    }
  })
  console.log("body__",body)

  const handleChange = (id, value) => {
    console.log(value.target.checked);
    //if(value.target.checked == true){
    props.data(id, value.target.checked);
    //}else{

    //}

  }

  const editTarget = (target, value) => {
    console.log(target);
    setTargetToEdit(target);
    setModalIsOpen(true)
  }
  const setModalIsOpenToFalse = () => {
    setModalIsOpen(false)
  }
  const showQuater = (value, event) => {
    console.log(value);
    console.log(event);
  }
  const hideQuater = (value, event) => {
    var head = [...heading];
    var subHead = [...subHeading];
    var data = [...tbody];
    //data.forEach( e=>{
    var index = 0;
    var segIndex = 0;
    data[0].Segment.forEach(seg => {
      for (var key in seg) {
        if (key === value) {
          segIndex = index
          console.log(seg)
        }
        index++;
      }
    })
    console.log(segIndex);
    data.forEach(e => {
      console.log(e.targetTemp[segIndex]);
      e.targetTemp[segIndex].hide = true;
    })
    console.log(data);
    setBody(data);
    var index = head.findIndex(h => h === value);
    if (index > -1) {
      head.splice(index, 1, value + ".");
    }
    // subHead.pop();
    // subHead.pop();
    // subHead.pop();
    // console.log(head);
    // console.log(subHead)
    // setHeading(head);
    // setSubHeading(subHead);
    // console.log(heading);
    // console.log(subHeading);
  }
  const sendDataToParent = (data) => { // the callback. Use a better name
    console.log("sendDataToParent", data);
    var segment = {};
    for (var key in data) {
      if (key.includes('CYT') || key.includes('PYT') || key.includes('Q4P') || key.includes('Comments') || key.includes('Comment')) {
        segment[key] = data[key];
        delete data[key];
      }
    }
    data["seg"] = segment;
    var arr = []
    for (var key in data.seg) {
      var target = key.split('CYT')
      var targetKey = target[0].trim();
      var obj = {}
      var temp = {};
      for (var key_temp in data.seg) {
        if (key_temp.includes(target[0])) {
          var tempKey;
          if (key_temp.includes('CYT')) {
            temp['CYT'] = data.seg[key_temp];
          } else if (key_temp.includes('PYT')) {
            temp['PYT'] = data.seg[key_temp];
          } else if (key_temp.includes('Q4P')) {
            temp['Q4P'] = data.seg[key_temp];
          } else if (key_temp.includes('Comments')) {
            temp['Comments'] = data.seg[key_temp];
          } else if (key_temp.includes('Comment')) {
            temp['Comment'] = data.seg[key_temp];
          }
          delete data.seg[key_temp];
        }
      }
      obj[targetKey] = temp;
      arr.push(obj);
      delete data.seg[key];
    }
    data["Segment"] = arr;
    delete data["seg"];
    console.log("After processing", data);
    let tableBody = [...body];
    var index = tableBody.findIndex(e => e.ID == data.ID);
    for (var i = 0; i < tableBody[index].Segment.length; i++) {
      for (var key in tableBody[index].Segment[i]) {
        data.Segment[i][key]['allowEdit'] = tableBody[index].Segment[i][key]['allowEdit'];
      }
    }
    if (index > -1) {
      tableBody.splice(index, 1, data);
      setBody(tableBody);
    }
    setModalIsOpenToFalse();
    //setDrive(index);
  };

  return (
    <div>
      <Table responsive>
        <thead>
          <tr>
            <th rowSpan="2">#</th>
            <th rowSpan="2">Edit Target</th>
            {heading.map((v) => {

              if (v === 'Category' || v === 'Metric Name' || v === 'Leading\/Lagging' || v === 'UOM' || v === 'ID' || v === 'Is_disabled') {
                if (v === 'Category') {
                  // return (<th className="fixedHeader" rowSpan="2">{v}</th>)
                  return (<th className="fixedHeader"
                    rowSpan="2">{v}</th>)
                } else if (v === 'Metric Name') {
                  return (<th className="fixedHeader" style={{ left: "74px" }} rowSpan="2">{v}</th>)
                } else if (v === 'Leading\/Lagging') {
                  return (<th className="fixedHeader" style={{ left: "154px" }} rowSpan="2">Lead/Lag</th>)
                } else if (v === 'UOM') {
                  return (<th className="fixedHeader" style={{ left: "228px" }} rowSpan="2">{v}</th>)
                } else {
                  return (<th rowSpan="2">{v}</th>)
                }

              } else {
                //   if(v.includes(".")){
                //     return (<th rowSpan="2">
                //     <button onClick={e=> showQuater(v,e)}>+</button>{v}
                //     </th>)
                // }else{
                //     return (<th colSpan="3">
                //     <button onClick={e=> hideQuater(v,e)}>-</button>{v}
                //     </th>)
                // }

                return (<th colSpan="3">{v}</th>)
              }

            })}
            
          </tr>
          <tr>
            {subHeading.map((sub) => {
              return (<th style={{ "top": "26px" }}> {sub} </th>)
            })}

          </tr>
        </thead>
        <tbody>
          {body.map((item) => {
            return (
              <tr>
                <td><input type="checkbox" value={item.ID} onChange={(value) => handleChange(item.ID, value)}></input></td>
                <td>{item.allowEdit && <button disabled={item.currentYearSelected} onClick={(value) => editTarget(item, value)}>Edit Targets</button>}
                  {!item.allowEdit && <button disabled>Edit Targets</button>}</td>
                {/* <td>{item.ID}</td> */}
                <td className="fixedRow">{item.Category}</td>
                <td className="fixedRow" style={{ left: "74px" }}>{item["Metric Name"]}</td>
                <td className="fixedRow" style={{ left: "154px" }}>{item["Leading/Lagging"]}</td>
                <td className="fixedRow" style={{ left: "228px" }}>{item.UOM}</td>

                {
                  item.targetTemp.map(seg => {
                    if (seg.hide) {
                      return (<td>-</td>)
                    } else {
                      return (<td colSpan="3" style={{ padding: "0" }}>
                        <div style={{ display: "grid", gridTemplateColumns: "33% 33% 33%" }}>
                        <Tooltip title= {(seg?.Comments?.length>0)?(seg?.Comments):("No Comments")}>
                          <div style={{ textAlign: "center" }}>
                            {seg.CYT}
                          </div></Tooltip>
                         <Tooltip title= {(seg?.Comments?.length>0)?(seg?.Comments):("No Comments")}> 
                         <div style={{ textAlign: "center" }}>{seg.PYT}</div></Tooltip>
                         <Tooltip title= {(seg?.Comments?.length>0)?(seg?.Comments):("No Comments")}>
                          <div style={{ textAlign: "center" }}>{seg.Q4P}</div></Tooltip>
                          
                        </div>
                        
                      </td>)
                    }

                  })
                }
                

              </tr>
            )
          })}

        </tbody>
      </Table>
      { modalIsOpen && <div className="popup-box">
        <div className="box">
          <div style={{ padding: "15px", backgroundColor: "#8080cf" }}>
            <span className="close-icon" onClick={setModalIsOpenToFalse}>x</span>
            <h3 style={{ margin: "0" }}>Edit Target</h3>
          </div>
          <EditTarget target={targetToEdit} sendDataToParent={sendDataToParent}
          setModalIsOpenToFalse={setModalIsOpenToFalse} />
        </div>
      </div> }
      
      
      {/* <Modal isOpen={modalIsOpen}>
        <div style={{ padding: "15px", backgroundColor: "#8080cf" }}>
          <span className="close-icon" onClick={setModalIsOpenToFalse}>x</span>
          <h3 style={{ margin: "0", "color": "white" }}>Edit Target</h3>
        </div>
        
      </Modal> */}
    </div>

  );
}

export default MetricTable;