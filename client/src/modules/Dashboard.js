import yearlyTarget from '../images/metric_list_yearly_target.png';
import metrixInput from '../images/metrix_insight_input.png';
import metrixView from '../images/metrix_insight_view.png';
import reportingLayout from '../images/reporting_layouts.png';
import reportingTimelines from '../images/reporting_timelines.png';
import rrMatrix from '../images/rr_matrix.png';
import home from '../home.png';
import OverlayTrigger from 'react-bootstrap/OverlayTrigger';
// import Tooltip from 'react-bootstrap/Tooltip';
import Tooltip from '@material-ui/core/Tooltip';
import HomeOutlinedIcon from '@mui/icons-material/HomeOutlined';
import DateRangeOutlinedIcon from '@mui/icons-material/DateRangeOutlined';
import DescriptionOutlinedIcon from '@mui/icons-material/DescriptionOutlined';
import ListAltOutlinedIcon from '@mui/icons-material/ListAltOutlined';
import AssignmentOutlinedIcon from '@mui/icons-material/AssignmentOutlined';
import PostAddOutlinedIcon from '@mui/icons-material/PostAddOutlined';
import GridOnOutlinedIcon from '@mui/icons-material/GridOnOutlined';
import { withStyles } from '@material-ui/styles';
import {
  BrowserRouter,
  useRouteMatch,
  Switch,
  Route,
  Link,
  useHistory
} from "react-router-dom";
import Home from './Home';
import RRMatrixTable from './rr-matrix/RRMatrixTable';
import InsightInput from './metricInsightInput/InsightInput';
import InsightView from './metricInsightView/InsightView';
import ReportingTimeline from './reportingTimeline/ReportingTimeline';
import ReportingLayout from './reportingLayout/ReportingLayout';
import LandingPage from './LandingPage';

const LightTooltip = withStyles((theme) => ({
  tooltip: {
    backgroundColor: '#484848',
    color: 'white',
    // boxShadow: theme.shadows[1],
    fontSize: 12,
  },
}))(Tooltip);

function Dashboard(props) {

  const { path } = useRouteMatch();
  const history = useHistory();
  const optionHeading = {
    margin: "0",
    fontWeight: "400"
  }
  const optionImage = {
    width: "100%",
    // border: "1px solid",
    // padding: "10px",
    // borderRadius: "8px",
    cursor: "pointer",
    margin: "10px 0"
  }
  return (

    // <div>
    //   <h2 style={{textAlign:"center"}}>Main Menu</h2>
    //   <div className="grid-container">
    //     <div className="grid-item">
    //       <img src={yearlyTarget} style={optionImage} onClick={()=> props.history.push('/metricList')}></img>
    //       <h6 style={optionHeading}>Metrics List + Yearly Targets</h6>
    //     </div>
    //     <div className="grid-item" onClick={()=> props.history.push('/rr_Matrix')}>
    //     <img src={rrMatrix} style={optionImage} ></img>
    //     <h6 style={optionHeading}>R&R Matrix</h6>
    //     </div>
    //     <div className="grid-item" >
    //     <img src={metrixView} style={optionImage} ></img>
    //     <h6 style={optionHeading}>Metrics + Insight View</h6>
    //     </div>  
    //     <div className="grid-item" onClick={()=> props.history.push('/insightInput')}>
    //     <img src={metrixInput} style={optionImage}></img>
    //     <h6 style={optionHeading}>Metrics + Insight Input</h6>
    //     </div>
    //     <div className="grid-item">
    //     <img src={reportingTimelines} style={optionImage}></img>
    //     <h6 style={optionHeading}>Reporting Timelines</h6>
    //     </div>
    //     <div className="grid-item">
    //     <img src={reportingLayout} style={optionImage}></img>
    //     <h6 style={optionHeading}>Report Layout</h6></div>  

    //   </div>
    // </div>
    <BrowserRouter>
      <div style={{
        width: "3%",
        float: "left",
        padding: "5px 10px",
        height: "85vh",
        borderRight: "1px solid #e2dede"
      }}>
        <nav>
          <ul style={{ listStyle: "none", padding: "4px" }}>
            <li onClick={() => history.push('/mainMenu')}>
              <Link to="/mainMenu">
                <LightTooltip title="Dashboard" >
                  <HomeOutlinedIcon style={{ fontSize: '48', color: 'black', padding: '6px 0px', marginLeft: '-5px' }} />
                </LightTooltip>
              </Link>
            </li>

            <li onClick={() => history.push('/dashboard/metricList')}>
              <Link to="/dashboard/metricList">
                <LightTooltip title="Metrics Annual Targets" >
                  <AssignmentOutlinedIcon style={{ fontSize: '48', color: 'black', padding: '6px 0px', marginLeft: '-5px' }} />
                </LightTooltip>
              </Link>
            </li>

            <li onClick={() => history.push('/dashboard/rr_matrix')}>
              <Link to="/dashboard/rr_matrix">
                <LightTooltip title="Metrics Roles & Responsibility Matrix" >
                  <GridOnOutlinedIcon style={{ fontSize: '45', color: 'black', padding: '6px 0px', marginLeft: '-4px' }} />
                </LightTooltip>
              </Link>
            </li>

            <li onClick={() => history.push('/dashboard/metricInput')}>
              <Link to="/dashboard/metricInput">
                <LightTooltip title="Metrics Data & Insights Input" >
                  <PostAddOutlinedIcon style={{ fontSize: '48', color: 'black', padding: '6px 0px', marginLeft: '-5px' }} />
                </LightTooltip>
              </Link>
            </li>

            <li onClick={() => history.push('/dashboard/metricView')}>
              <Link to="/dashboard/metricView">
                <LightTooltip title="Metrics Data & Insight Output" >
                  <ListAltOutlinedIcon style={{ fontSize: '48', color: 'black', padding: '6px 0px', marginLeft: '-5px' }} />
                </LightTooltip>
              </Link>
            </li>

            <li onClick={() => history.push('/dashboard/reportingTimeline')}>
              <Link to="/dashboard/reportingTimeline">
                <LightTooltip title="Reporting Timelines" >
                  <DateRangeOutlinedIcon style={{ fontSize: '48', color: 'black', padding: '6px 0px', marginLeft: '-5px' }} />
                </LightTooltip>
              </Link>
            </li>

            <li onClick={() => history.push('/dashboard/reportingLayout')}>
              <Link to="/dashboard/reportingLayout">
                <LightTooltip title="Metrics Report" >
                  <DescriptionOutlinedIcon style={{ fontSize: '48', color: 'black', padding: '6px 0px', marginLeft: '-5px' }} />
                </LightTooltip>
              </Link>
            </li>
          </ul>
        </nav>
      </div>

      <Switch>
        <div style={{
          width: "93%",
          float: "left",
          padding: "5px 10px"
        }}>
          <Route path={`mainMenu`}>
            <LandingPage />
          </Route>
          <Route path={`${path}/metricList`}>
            <Home />
          </Route>
          <Route path={`${path}/rr_matrix`}>
            <RRMatrixTable />
          </Route>
          <Route path={`${path}/metricInput`}>
            <InsightInput />
          </Route>
          <Route path={`${path}/metricView`}>
            <InsightView />
          </Route>
          <Route path={`${path}/reportingTimeline`}>
            <ReportingTimeline />
          </Route>
          <Route path={`${path}/reportingLayout`}>
            <ReportingLayout />
          </Route>
        </div>

      </Switch>
    </BrowserRouter>


  );



}

export default Dashboard;