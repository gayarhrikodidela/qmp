import React, { Component } from "react";
class Footer extends Component {
  render() {
      const footer = {
        width: "100%",
        position: "fixed",
        background: "#0000a0",
        bottom: "0",
      }
      const text = {
        margin: "5px",
        float: "right",
        color: "white",
        fontWeight: "500"
      }
    return(
        <div style={footer}>
            <h4 style={text}>CONFIDENTIAL PROPERTY OF MARS INC. DO NOT DISTRIBUTE</h4>
        </div>
    );
  }
}
 
export default Footer;