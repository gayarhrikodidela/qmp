import React, { Component } from "react";
import AddMetricPopup from './AddMetricPopup'
import AddSegment from './AddSegment'
import AddNewRole from "./AddNewRole";
import DeleteSegment from "./DeleteSegment";
import MetricTable from "./MetricTable";
import HomeService from "../services/HomeService";
import Common from '../utils/Common';
import { css } from "@emotion/react";
import Select from 'react-select';
import MetricNameDropDown from "./MetricNameDropDown";
//import { useHistory } from 'react-router-dom';

class Home extends React.Component {

  constructor() {
    super();
    this.state = {
      addMetricIsOpen: false,
      addSegmentIsOpen: false,
      addNewRoleIsOpen: false,
      deleteSegmentIsOpen: false,
      showCopyMetrix: false,
      metrixToBeDeleted: [],
      tableBody: [],
      productCategory: [],
      serviceCategory: [],
      heading: [],
      segment: [],
      body: [],
      newMetric: [],
      user: [],
      year: [],
      currentYearSelected: false,
      selectedYear: '',
      loading: true,
      metricNameList : [],
      changedYearOrType: false,
      selectedMetricType:"Product",
      isAdmin: false,
      isTargetDefination: false,
      assignedMetric:[],
      assignedSegment:[],
      targetDefination:{}
    }
    this.toggleAddMetricPopup = this.toggleAddMetricPopup.bind(this);
    this.toggleAddSegmentPopup = this.toggleAddSegmentPopup.bind(this);
    this.toggleAddNewRole = this.toggleAddNewRole.bind(this);
    this.toggleDeleteSegmentPopup = this.toggleDeleteSegmentPopup.bind(this);
    this.setLoading = this.setLoading.bind(this);
    this.deleteMetrix = this.deleteMetrix.bind(this);
    this.processTable = this.processTable.bind(this);
    this.copyMetrix = this.copyMetrix.bind(this);
    this.filterDataByYear = this.filterDataByYear.bind(this);
    this.filterDataByMetricName = this.filterDataByMetricName.bind(this);
   // this.loginPage = new goToLoginPage();
    this.homeService = new HomeService();
    this.commonSerice = new Common();

  }
  componentDidMount() {
    this.getMetrixData();
    this.getYearList();
    this.checkCopyMetrixButton();
    this.getCategoryList();
    this.commonSerice.isAdmin().then(role =>{
      console.log("User role is admin:"+role);
      if(role === "true"){
        this.setState({isAdmin: true});
      }else{
        this.commonSerice.getUser().then( user =>{
          console.log("Target Defination role");
          var segmentAssigned= [];
          var metricAssigned = [];
          var targetDefination = {};
           for(var key in user.roles){
             if(key === "Target Definition"){
              targetDefination = user.roles[key];
                  for(var target_key in user.roles[key]){
                    console.log(target_key);
                    var metric = user.roles[key][target_key];
                    metric.forEach(e=>{
                      metricAssigned.push(e);
                    })
                    segmentAssigned.push(target_key);

                  }
             }
           }
           console.log("Segments Assigned:",segmentAssigned)
           console.log("Metric Assigned:",metricAssigned)
           this.setState({isTargetDefination: true, assignedMetric:metricAssigned,assignedSegment:segmentAssigned,targetDefination:targetDefination  })
        })
      }
      
    })
  }
  getYearList() {
    this.setState({ selectedYear: new Date().getFullYear() })
    this.homeService.getYearFilter().then(item => {
      if (item.status == 'Success') {
        let data = JSON.parse(item.data);
        console.log("Year", data);
        this.setState({ ...this.state, year: data });
      }
    })
  }
  getCategoryList(){
    this.homeService.getCategoryList().then(item => {
      if (item.status == 'Success') {
        let data = JSON.parse(item.data);
        console.log("Category List", data);
        var pCategory = [];
        var sCategory = [];
        data.Product.forEach(e=>{
          pCategory.push({ value: e, label: e })
        })
        data.Service.forEach(e=>{
          sCategory.push({ value: e, label: e })
        })
        this.setState({...this.state, productCategory: pCategory, serviceCategory: sCategory})
       // this.setState({ ...this.state, year: data });
      }
    })
  }
  filterDataByYear(event) {
    this.setState({changedYearOrType: true});
    this.setLoading();
    console.log(event.target.value);
    var year;
    var metricType;
    if(event.target.value === "Service" || event.target.value === "Product"){
      this.setState({ selectedMetricType: event.target.value });
     metricType = event.target.value
     year = this.state.selectedYear;
    }else{
      this.setState({ selectedYear: event.target.value });
      metricType = this.state.selectedMetricType
      year = event.target.value;
      var currentYear = new Date().getFullYear();
      if (currentYear != event.target.value) {
        this.setState({ currentYearSelected: true });
      } else {
        this.setState({ currentYearSelected: false });
      }
    }
    console.log(this.state.selectedYear+""+this.state.selectedMetricType)
    this.homeService.filterDataByYear(year,metricType).then(item => {
      this.setState({changedYearOrType: false });
      if (item.status == 'Success') {
        let data = JSON.parse(item.data);
        this.setLoading();
        console.log("Filter data by year", data);
        this.setState({ ...this.state, body: data })
        this.processTable(this.state.body);
      }else{
        this.setLoading();
      }
    })
  }
  filterDataByMetricName(value){
     console.log(value);
    const tableBody = [...this.state.body];
    var filteredData = [];
    // var selectedMetricName = [];
    // value.forEach(e=>{
    //   selectedMetricName.push(e);
    // })
    if(value.length > 0){
      tableBody.forEach( e=>{
        value.forEach(metricName =>{
          if(e["Metric Name"] === metricName.value){
            filteredData.push(JSON.parse(JSON.stringify(e)));
          }
        })
        
      })
    }else{
      filteredData = [...this.state.body];
    }
    
    console.log(filteredData);
    filteredData.forEach(e => {
      var segment = {};
      for (var key in e) {
        if (key.includes('CYT') || key.includes('PYT') || key.includes('Q4P') || key.includes('Comments') || key.includes('Comment')) {
          segment[key] = e[key];
          delete e[key];
        }
      }
      e["seg"] = segment;
      if (this.state.isAdmin === true) {
        e["allowEdit"] = true;
      }else if(this.state.isTargetDefination === true){
        var index = this.state.assignedMetric.findIndex(metric=> metric === e.ID )
        if(index > -1){
          e["allowEdit"] = true;
        }else{
          e["allowEdit"] = false;
        }
      }else{
        e["allowEdit"] = false;
      }
      e["currentYearSelected"] = this.state.currentYearSelected;
    })
    filteredData.forEach(e => {
      var arr = []
      for (var key in e.seg) {
        var target = key.split('CYT')
        var targetKey = target[0].trim();
        var obj = {}
        var temp = {};
        for (var key_temp in e.seg) {
          if (key_temp.includes(target[0])) {
            var tempKey;
            if (key_temp.includes('CYT')) {
              temp['CYT'] = e.seg[key_temp];
            } else if (key_temp.includes('PYT')) {
              temp['PYT'] = e.seg[key_temp];
            } else if (key_temp.includes('Q4P')) {
              temp['Q4P'] = e.seg[key_temp];
            } else if (key_temp.includes('Comments')) {
              temp['Comments'] = e.seg[key_temp];
            } else if (key_temp.includes('Comment')) {
              temp['Comment'] = e.seg[key_temp];
            }
            
            if (this.state.isAdmin === true) {
              temp['allowEdit'] = true;
            } else if(this.state.isTargetDefination === true){
              var index = this.state.assignedSegment.findIndex(segment=> key_temp.includes(segment) )
              if(index > -1){
                temp["allowEdit"] = true;
              }else{
                temp["allowEdit"] = false;
              }
            } else {
              temp['allowEdit'] = false;
            }
            delete e.seg[key_temp];
          }
        }
        obj[targetKey] = temp;
        arr.push(obj);
        delete e.seg[key];
      }
      e["Segment"] = arr;
      delete e["seg"];
    })
    this.setState({tableBody: filteredData});
  }
  checkCopyMetrixButton() {
    let date = new Date().getMonth();
    if (date + 1 >= 7) {
      this.setState({ showCopyMetrix: true });
    }
  }
  getMetrixData() {
    
    this.homeService.getMetrixData().then(item => {
      if (item.status == 'Success') {
        this.setLoading();
        let data = JSON.parse(item.data);
        console.log("Real data");
        console.log(item.data);
        this.setState({ ...this.state, body: data })
        this.processTable(this.state.body);
      }else{
        this.setLoading();
      }

    });
  }
  deleteMetrix() {
    console.log(this.state.metrixToBeDeleted)
    this.setLoading();
    this.homeService.deleteMetrix(this.state.metrixToBeDeleted).then(item => {
      if (item?.status === 'Success') {
        this.setLoading();
        var tbody = [...this.state.body];
        this.state.metrixToBeDeleted.forEach(e => {
          var index = tbody.findIndex(m => m.ID === e);
          if (index > -1) {
            tbody.splice(index, 1);
          }
        })
        this.setState({ ...this.state, body: tbody })
        this.processTable(tbody);
      }else{
        this.setLoading();
      }
    });

  }
  toggleAddMetricPopup() {
    this.setState({ addMetricIsOpen: !this.state.addMetricIsOpen});
  }
  setLoading() {
    this.setState({ loading: !this.state.loading});
  }
  toggleAddSegmentPopup() {
    this.setState({ addSegmentIsOpen: !this.state.addSegmentIsOpen});
  }
  toggleAddNewRole(newMetric) {
    this.setState({ ...this.state, newMetric: newMetric})
    // this.setState({addMetricIsOpen: !this.state.addMetricIsOpen});
    this.setState({ addNewRoleIsOpen: !this.state.addNewRoleIsOpen });
  }
  toggleDeleteSegmentPopup() {
    this.setState({ deleteSegmentIsOpen: !this.state.deleteSegmentIsOpen});
  }
  copyMetrix() {
    this.setLoading();
    this.homeService.copyMetrix().then(item => {
      this.setLoading();
      console.log("copy metrix", item);
    })
  }
  update(value,checked) {
    if(checked === true){
      this.state.metrixToBeDeleted.push(value);
    }else{
      var metrix = [...this.state.metrixToBeDeleted];
      var index = metrix.findIndex(m => m == value);
      if(index > -1){
        metrix.splice(index, 1);
      }
      this.setState({metrixToBeDeleted: metrix})
    }
    console.log(this.state.metrixToBeDeleted);
    //this.state.metrixToBeDeleted.push(value);
  }
  handelDeleteSegment(segment) {
    let data = [...this.state.body];
    this.setState({ deleteSegmentIsOpen: !this.state.deleteSegmentIsOpen});
    this.setLoading();
    this.homeService.deleteSegment(segment).then(item => {
      console.log("getMetrixData", item);
      this.setLoading();
      if (item.status === "Success") {
        
        data.forEach(e => {
          for (var key in e) {
            if (key.includes(segment.segment)) {
              delete e[key];
            }
          }
        })
        this.setState({ ...this.state, body: data })
        this.processTable(data);
      }
    });

   
  }
  saveNewMetric(newMetric) {
    this.setState({ ...this.state, newMetric: newMetric })
    this.setLoading();
    this.homeService.saveMetrix(newMetric).then(item => {
      console.log("getMetrixData", item);
      if (item.status === "Success") {
        this.setLoading();
        let data = JSON.parse(item.data);
        let body = [...this.state.body];
        body.push(data[0]);
        body.sort((a, b) => {
          let fa = a.Category.toLowerCase(),
            fb = b.Category.toLowerCase();
          if (fa < fb) {
            return -1;
          }
          if (fa > fb) {
            return 1;
          }
          return 0;
        });
        if(data[0]["Product/Service"] === this.state.selectedMetricType){
          this.setState({ ...this.state, body: body })
          this.processTable(body);
        }
        
      }else{
        this.setLoading();
      }
    });
    if (this.state.addNewRoleIsOpen === true) {
      this.setState({ addNewRoleIsOpen: !this.state.addNewRoleIsOpen });
    }
    if (this.state.addMetricIsOpen === true) {
      //this.toggleAddMetricPopup();
      this.setState({ addMetricIsOpen: !this.state.addMetricIsOpen });
    }


  }
  saveNewSegment(newSegment) {
    this.setLoading();
    this.homeService.saveSegment(newSegment).then(item => {
      console.log("saveNewSegment", item);
      if (item.status === "Success") {
        this.setLoading();
        let data = [...this.state.body];
        data.forEach(e => {
          e[newSegment.segment + " CYT"] = null;
          e[newSegment.segment + " PYT"] = null
          e[newSegment.segment + " Q4P"] = null
          e[newSegment.segment + " Comments"] = newSegment.comment
        })
        this.setState({ ...this.state, body: data })
        this.processTable(data);
      }else{
        this.setLoading();
      }
    });
    this.toggleAddSegmentPopup();


  }
  processTable(data) {
    const tableBody = JSON.parse(JSON.stringify(data));
    
    tableBody.forEach(e => {
      var segment = {};
      for (var key in e) {
        if (key.includes('CYT') || key.includes('PYT') || key.includes('Q4P') || key.includes('Comments') || key.includes('Comment')) {
          segment[key] = e[key];
          delete e[key];
        }
      }
      e["seg"] = segment;
     
      if (this.state.isAdmin === true) {
        e["allowEdit"] = true;
      }else if(this.state.isTargetDefination === true){
        // var allowEdit = false;
        //   for(var insigthKey in this.state.targetDefination){
        //     if(insigthKey === e.Segment){
        //       var metric =  this.state.targetDefination[insigthKey];
        //       var index = metric.findIndex(e=> e === data.Metric_ID)
        //       if(index > -1){
        //         allowEdit = true;
        //       }
        //     }
        // }
        // e["allowEdit"] = allowEdit;
        var index = this.state.assignedMetric.findIndex(metric=> metric === e.ID )
        if(index > -1){
          e["allowEdit"] = true;
        }else{
          e["allowEdit"] = false;
        }
      }else {
        e["allowEdit"] = false;
      }
      e["currentYearSelected"] = this.state.currentYearSelected;
    })
    tableBody.forEach(e => {
      var arr = []
      for (var key in e.seg) {
        var target = key.split('CYT')
        var targetKey = target[0].trim();
        var obj = {}
        var temp = {};
        for (var key_temp in e.seg) {
          if (key_temp.includes(target[0])) {
            var tempKey;
            if (key_temp.includes('CYT')) {
              temp['CYT'] = e.seg[key_temp];
            } else if (key_temp.includes('PYT')) {
              temp['PYT'] = e.seg[key_temp];
            } else if (key_temp.includes('Q4P')) {
              temp['Q4P'] = e.seg[key_temp];
            } else if (key_temp.includes('Comments')) {
              temp['Comments'] = e.seg[key_temp];
            } else if (key_temp.includes('Comment')) {
              temp['Comment'] = e.seg[key_temp];
            }
            if (this.state.isAdmin === true) {
              temp["allowEdit"] = true;
            } 
            else if(this.state.isTargetDefination === true){
              var allowEdit = false;
              for(var insigthKey in this.state.targetDefination){
                if(key_temp.includes(insigthKey)){
                  var metric =  this.state.targetDefination[insigthKey];
                  var index = metric.findIndex(m=> m === e.ID)
                  if(index > -1){
                    allowEdit = true;
                  }
                }
            }
            temp["allowEdit"] = allowEdit;
              // console.log(key_temp);
              // var index = this.state.assignedSegment.findIndex(segment=> key_temp.includes(segment) )
              // if(index > -1){
              //   temp["allowEdit"] = true;
              // }else{
              //   temp["allowEdit"] = false;
              // }
            }
            else {
              temp["allowEdit"] = false;
            }
            delete e.seg[key_temp];
          }
        }
        obj[targetKey] = temp;
        arr.push(obj);
        delete e.seg[key];
      }
      e["Segment"] = arr;
      delete e["seg"];
    })
    //this.state.tableBody = tableBody



    // const category = [];
    // this.state.body.forEach(e => {
    //   if (!category.includes(e.Category)) {
    //     const found = category.find(c => c.value == e.Category);
    //     if (!found) {
    //       category.push({ value: e.Category, label: e.Category });
    //     }

    //   }

    // })
    var metricList=[];
    tableBody.forEach(e=>{
        metricList.push({ label: e["Metric Name"] , value: e["Metric Name"]});
    })

    const heading = []
      const segment = []

    for (var key in tableBody[0]) {
      if (key === 'Segment') {
        tableBody[0].Segment.forEach(seg => {
          for (var key in seg) {
            heading.push(key);
            segment.push(key);
          }
        })
      }
    }
    this.setState({ ...this.state, heading: heading, segment: segment, tableBody: tableBody, metricNameList:metricList })
    console.log("Home 2: ", this.state.tableBody);

  }
  render() {
    const yearDropDownDiv = {
      display: "inline-block",
      backgroundColor: "#0000a0",
      padding: "5px",
      color: "rgb(255, 255, 255)",
      width: "100%",
      margin: "10% 0"
    }
    const yearDropDown = {
      width: "100%",
      padding: "3px"
    }
    return (

      <div >
        { this.state.loading && <div className="loader-box">
        <div id="loader"></div>
        </div>}
        
        <div style={{
          width: "86%",
          float: "left",
          padding: "5px 10px"
        }}>
          {
            this.state.isAdmin === true &&
            <div>
            <button disabled={this.state.currentYearSelected} onClick={this.toggleAddMetricPopup}>Add New Metrics</button>
            <button disabled={this.state.currentYearSelected} onClick={this.deleteMetrix}>Delete Metric</button>
            {this.state.showCopyMetrix && <button style={{float: "right"}} disabled={this.state.currentYearSelected} onClick={this.copyMetrix}>Copy Metrics</button>}
            {!this.state.showCopyMetrix && <button disabled>Copy Metrics</button>}
            <button disabled={this.state.currentYearSelected} onClick={this.toggleAddSegmentPopup}>Add New Segment</button>
            <button disabled={this.state.currentYearSelected} onClick={this.toggleDeleteSegmentPopup}>Delete Segment</button>
          </div>
          }
         

          {this.state.addMetricIsOpen && <AddMetricPopup
            productCategory={this.state.productCategory}
            serviceCategory={this.state.serviceCategory}
            handleClose={this.toggleAddMetricPopup}
            addNewRole={this.toggleAddNewRole.bind(this)}
            saveNewMetric={this.saveNewMetric.bind(this)}

          />}
          {this.state.addNewRoleIsOpen && <AddNewRole
            newMetric={this.state.newMetric}
            heading={this.state.heading}
            handleClose={this.toggleAddNewRole}
            saveNewMetric={this.saveNewMetric.bind(this)}
          />}

          <div style={{ overflow: "auto", height: "75vh" }}>
            <MetricTable body={this.state.tableBody} data={this.update.bind(this)}
            ></MetricTable>
          </div>
          {/* <div style={{ textAlign: "center" }}>
            <button onClick={this.toggleAddSegmentPopup}>Add New Segment</button>
            <button onClick={this.toggleDeleteSegmentPopup}>Delete Segment</button>
          </div> */}
          {this.state.deleteSegmentIsOpen && <DeleteSegment
            handleClose={this.toggleDeleteSegmentPopup}
            segment={this.state.segment}
            deleteSeg={this.handelDeleteSegment.bind(this)}
          />}
          {this.state.addSegmentIsOpen && <AddSegment
            handleClose={this.toggleAddSegmentPopup}
            metricType = {this.state.selectedMetricType}
            saveNewSegment={this.saveNewSegment.bind(this)}
          />}
        </div>
        <div style={{
          width: "10%",
          float: "left",
          padding: "5px 10px"
        }}>

          <h2 style={{ textAlign: "center", margin: "0" }}>Filters</h2>
          <div style={yearDropDownDiv}>
            <label style={{display: "block"}}>
              Year
            </label>
            <select style={yearDropDown} onChange={this.filterDataByYear} value={this.state.selectedYear}>
              {
                this.state.year.map((y) => {
                  return (<option key={y} value={y}>{y}</option>)
                })
              }
            </select>
          </div>
          <div style={{display: "inline-block",
                      backgroundColor: "#0000a0",
                      padding: "5px",
                      width: "100%",
                      margin: "10% 0"}}>
            <label style={{display: "block", color: "white"}}>
              Metric Name
            </label>
            <MetricNameDropDown  metricNameList={this.state.metricNameList}
            resetSelectedMetric={this.state.changedYearOrType}
            filterMetricByName ={this.filterDataByMetricName.bind(this)} ></MetricNameDropDown>
            
          </div>
          <div style={yearDropDownDiv}>
            <label style={{display: "block"}}>
              Product/Service
            </label>
            <select style={yearDropDown} onChange={this.filterDataByYear} value={this.state.selectedMetricType}>
              <option value="Product">Product</option>
              <option value="Service">Service</option>
            </select>
          </div>
        </div>
      </div>
    );
  }
}

export default Home;