import appStyle from './App.css';
import React, { useEffect } from 'react';
import Footer from './modules/Footer';
import LandingPage from './modules/LandingPage';
import Dashboard from './modules/Dashboard';
import Login from './modules/Login';
import Header from './modules/Header';
import PrivateRoute from './utils/PrivateRoute';
import PublicRoute from './utils/PublicRoute';
import CreateUser from './modules/persona/CreateUser';
import ViewUser from './modules/persona/ViewUser';
import AuthProvider from '../src/services/AuthProvider';
import loginPhoto from './images/loginPhoto.png'
import { msalConfig, msalApp, requiresInteraction, isIE } from './services/msalConfig';
import {
  BrowserRouter,
  Route,
  Switch
} from "react-router-dom";

function App(props) {

  const [acc, setState] = React.useState({
    account: null,
  })

  React.useEffect(() => {
    // sessionStorage.setItem("sideNav", true);
    props.onSignIn()
  }, [])

  React.useEffect(() => {
    if (props.account) {
      console.log("The props in app.js is ", props);
      setState({ account: props.account });
    }

  }, [props.account])


  function onSignOut() {
    msalApp.logout();    
    localStorage.removeItem('mars.access_token');
    localStorage.removeItem('mars.refresh_token');
  }

  return (
    <div>
      {props.account ? (
        <React.StrictMode>
          <BrowserRouter>
            <Header onSignOut={onSignOut} />
            <Switch>
              <Route exact path='/' component={Login} />
              <Route exact path='/login' component={Login} />
              <Route exact path='/createUser' component={CreateUser} />
              <Route exact path='/viewUser' component={ViewUser} />
              <PrivateRoute path='/mainMenu' component={LandingPage} />
              <PrivateRoute path='/dashboard' component={Dashboard} />
            </Switch>
            <Footer />
          </BrowserRouter>
        </React.StrictMode>
      ) : (
        <img style={{ width: '100%', height: '100%' }} src={loginPhoto} />
      )}
    </div>
  );
}

export default AuthProvider(App);
