// // import { PublicClientApplication } from '@azure/msal-browser';
import {UserAgentApplication} from 'msal';

import { PublicClientApplication } from '@azure/msal-browser';
export const msalConfig = {
  appId: 'b1ee6f15-7efe-4935-bcba-4b6ab578e858',
  tenantId: '2fc13e34-f03f-498b-982a-7cb446e25bc6',
  authority:'https://login.microsoftonline.com/2fc13e34-f03f-498b-982a-7cb446e25bc6',
  redirectUri: 'https://localhost:3000/',
  scopes: ['openid', 'email', 'profile'],
};
 
export const requiresInteraction = errorMessage => {
  if (!errorMessage || !errorMessage.length) {
    return false;
  }
  return (
    errorMessage.indexOf('consent_required') > -1 ||
    errorMessage.indexOf('interaction_required') > -1 ||
    errorMessage.indexOf('login_required') > -1
  );
};
 
export const isIE = () => {
  const ua = window.navigator.userAgent;
  const msie = ua.indexOf('MSIE ') > -1;
  const msie11 = ua.indexOf('Trident/') > -1;
 
  // If you as a developer are testing using Edge InPrivate mode, please add "isEdge" to the if check
  // const isEdge = ua.indexOf("Edge/") > -1;
 
  return msie || msie11;
};
 
export const msalApp = new PublicClientApplication({
  auth: {
    clientId: msalConfig.appId,
    authority: msalConfig.authority,
    redirectUri: msalConfig.redirectUri,
  },
  cache: {
    cacheLocation: 'localStorage',
    storeAuthStateInCookie: true,
  },
});