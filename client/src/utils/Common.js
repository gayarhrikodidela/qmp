class Common{
// return the user data from the session storage
   async getUser() {
    const userStr = sessionStorage.getItem('user');
    if (userStr) return JSON.parse(userStr);
    else {
      return null;
    }
  }
   
  //return if the user is Admin or not
  async isAdmin() {
    const userRole = sessionStorage.getItem('isAdmin');
    if (userRole) return userRole;
    else {
      return null;
    }
  }

  // return the token from the session storage
  async getToken(){
    var token = sessionStorage.getItem('token');
    if(token) return JSON.parse(token);
  }
   
  // remove the token and user from the session storage
  async removeUserSession() {
    sessionStorage.removeItem('token');
    sessionStorage.removeItem('user');
  }
   
  // set the token and user from the session storage
  async setUserSession (token, user) {
    sessionStorage.setItem('token', JSON.stringify(token));
    sessionStorage.setItem('user', JSON.stringify(user));
    var isAdminRole = false;
    for(var key in user.roles){
      if(key === "Corporate System ADMIN" || key === "Corporate Admin"){
        isAdminRole = true;
      }
    }
    sessionStorage.setItem('isAdmin', isAdminRole);
  }
}
export default Common;

