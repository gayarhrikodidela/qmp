import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import Common from './Common';
 
// handle the private routes
function PrivateRoute({ component: Component, ...rest }) {
    const commonService = new Common;
    let token = "";
    commonService.getToken().then(item =>{
        token = item;
        console.log("Private Route",token);
        console.log(token !== null)
    })
  return (
    <Route
      {...rest}
      render={(props) => token !== null ? <Component {...props} /> : <Redirect to={{ pathname: '/login', state: { from: props.location } }} />}
    />
  )
}
 
export default PrivateRoute;