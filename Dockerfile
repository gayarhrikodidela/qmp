# Frontend build
FROM node:10.16.0-alpine AS frontend
WORKDIR /app
COPY client/package.json /app/package.json
RUN npm install --silent
# add `/app/node_modules/.bin` to $PATH
ENV PATH /app/node_modules/.bin:$PATH
#COPY client/.env /app/.env
COPY client/src /app/src
COPY client/public /app/public

RUN npm run build


FROM ubuntu:18.04

# RUN app-get update
COPY --from=frontend /app/build /src/build
COPY server/ /src/
WORKDIR /src
ENV FLASK_APP "main"
ENV FLASK_ENV "production"
RUN apt-get update -y && apt-get install -y --no-install-recommends python3.7 git python3-pip gcc libc6-dev unixodbc unixodbc-dev  build-essential curl
RUN apt-get install -y gnupg2
RUN apt-get install -y python3-dev python3-setuptools

# Add SQL Server ODBC Driver 17 for Ubuntu 18.04
RUN curl https://packages.microsoft.com/keys/microsoft.asc | apt-key add -
RUN curl https://packages.microsoft.com/config/ubuntu/18.04/prod.list > /etc/apt/sources.list.d/mssql-release.list
RUN apt-get update

RUN apt install ca-certificates

RUN ACCEPT_EULA=Y apt-get install -y --allow-unauthenticated msodbcsql17
RUN ACCEPT_EULA=Y apt-get install -y --allow-unauthenticated mssql-tools
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bash_profile
RUN echo 'export PATH="$PATH:/opt/mssql-tools/bin"' >> ~/.bashrc

RUN python3 -m pip install --upgrade pip
RUN python3 -m pip install --no-cache-dir -r requirements.txt

WORKDIR /src/src

EXPOSE 8000

RUN python3 -m pip install gunicorn

CMD gunicorn --bind 0.0.0.0:8000 --workers=4 -t 600 app:app 